package com.danielhoop.StringTemplateTest;

import org.apache.commons.text.StringSubstitutor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        // Use string templates more easily with ${}
        // https://commons.apache.org/proper/commons-text/apidocs/org/apache/commons/text/StringSubstitutor.html

        // This string is the template defined by the admin for the table
        // This information is stored in a dedicated database table and has to be fetched from the database and then stored in an appropriate collection such as a list.
        List<TableEntry> table = new ArrayList<>(){};
        table.add(new TableEntry("P410.0100.10000", "${zeile}, ${spalte}", "${zeile_einheit}", "", ""));
        table.add(new TableEntry("P410.0100.32000", "${zeile}, ${spalte} (${kontext})", "${zeile_einheit}", "", ""));

        // Get all labels for Merkmale in a table by some kind of a method call...
        // The method call is not implemented here, it's only a mock map containing two entries.
        // This should be done in *one* database query for all merkmale in the table (see above) in order to avoid database access for each row in the table.
        HashMap<String, HashMap<String, String>> labels = new HashMap<>();
        HashMap<String, String> label;
        label = new HashMap<>();
        label.put("zeile", "Anlagevermögen");
        label.put("spalte", "31.12.");
        label.put("kontext", "Bilanz");
        label.put("zeile_einheit", "CHF");
        labels.put("P410.0100.32000", label);
        //
        label = new HashMap<>();
        label.put("zeile", "Aktiven");
        label.put("spalte", "31.12.");
        label.put("kontext", "Bilanz");
        label.put("zeile_einheit", "CHF");
        labels.put("P410.0100.10000", label);

        // Now, go through all entries in the table and return the generated front end labels.
        // The result should be stored in a list and returned to the front end, but for simplicity it's just printed here.
        for (TableEntry entry: table) {
            HashMap<String, String> templMap = new HashMap<>();
            templMap.put("zeile",  labels.get(entry.getMerkmal()).get("zeile"));
            templMap.put("spalte",  labels.get(entry.getMerkmal()).get("spalte"));
            templMap.put("kontext",  labels.get(entry.getMerkmal()).get("kontext"));
            templMap.put("zeile_einheit",  labels.get(entry.getMerkmal()).get("zeile_einheit"));
            StringSubstitutor ss = new StringSubstitutor(templMap);
            System.out.println(ss.replace(entry.getLeftTemplate1()) + "\t" + ss.replace(entry.getLeftTemplate2()) + "\t" +
                    ss.replace(entry.getRightTemplate1()) + "\t" + ss.replace(entry.getRightTemplate2()) + "\t");
        }

        // The usage of Java build in templates from Java 21 did not work...
        // String inBeschriftung = "${zeile}"
        // String outBeschriftung = inBeschriftung.replaceAll("\\$", "\\\\");
        // String zeile = "Anlagevermögen";
        // System.out.println(STR.process(outBeschriftung));
        // System.out.println(STR.process(StringTemplate.of(outBeschriftung)));
    }
}