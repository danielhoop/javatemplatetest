package com.danielhoop.StringTemplateTest;

public class TableEntry {
    private String merkmal;
    private String leftTemplate1 = "";
    private String leftTemplate2 = "";
    private String rightTemplate1 = "";
    private String rightTemplate2 = "";

    public String getMerkmal() {
        return merkmal;
    }

    public String getLeftTemplate1() {
        return leftTemplate1;
    }

    public String getLeftTemplate2() {
        return leftTemplate2;
    }

    public String getRightTemplate1() {
        return rightTemplate1;
    }

    public String getRightTemplate2() {
        return rightTemplate2;
    }

    public TableEntry(String merkmal, String leftTemplate1, String leftTemplate2, String rightTemplate1, String rightTemplate2) {
        this.merkmal = merkmal;
        this.leftTemplate1 = leftTemplate1;
        this.leftTemplate2 = leftTemplate2;
        this.rightTemplate1 = rightTemplate1;
        this.rightTemplate2 = rightTemplate2;
    }
}
